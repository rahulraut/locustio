FROM ubuntu:16.04
RUN apt-get update
RUN apt-get -y install git python-pip libevent-dev python-all-dev software-properties-common
RUN pip install locustio pyzmq

COPY start.sh /home/root/start.sh
RUN chmod +x /home/root/start.sh

EXPOSE 8089

ENTRYPOINT ["/home/root/start.sh"]