#!/bin/bash

GIT_TEST_REPO=${GIT_TEST_REPO:=}
GIT_REPO_NAME=${GIT_REPO_NAME:=}
LOCUST_LOCUSTFILE=${LOCUST_LOCUSTFILE:=}
LOCUST_TARGET_HOST=${LOCUST_TARGET_HOST:=}
LOCUST_NUM_REQUEST=${LOCUST_NUM_REQUEST:=}
LOCUST_USERS=${LOCUST_USERS:=}
LOCUST_HATCH_RATE=${LOCUST_HATCH_RATE:=}
LOCUST_OPTIONS="-f $LOCUST_LOCUSTFILE -H $LOCUST_TARGET_HOST TeamboardUser --no-web -c $LOCUST_USERS -r $LOCUST_HATCH_RATE --print-stats --only-summary --num-request=$LOCUST_NUM_REQUEST --logfile=results.log"

mkdir -p /home/root/test
cd /home/root/test
git clone $GIT_TEST_REPO
cd /home/root/test/$GIT_REPO_NAME

echo "Running..."
locust $LOCUST_OPTIONS

echo "Moving logs to Jenkinsdata..."
rsync -a /home/root/test/$GIT_REPO_NAME/ /var/jenkins_home/locustReports/